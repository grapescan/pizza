package me.grapescan.nennopizza.data.cart.storage

import android.annotation.SuppressLint
import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.*
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.data.entity.Product
import java.lang.reflect.Type

class PrefCartStorage(context: Context) : CartStorage {

    companion object {
        private const val PREF_CART = "cart"
    }

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)
    private val gson = GsonBuilder()
            .registerTypeAdapter(Cart::class.java, CartTypeAdapter())
            .create()

    @SuppressLint("ApplySharedPref")
    override fun save(items: List<Product>) {
        val dataString = gson.toJson(Cart(items))
        prefs.edit().putString(PREF_CART, dataString).commit()
    }

    override fun load(): List<Product> {
        val dataString = prefs.getString(PREF_CART, "[]")
        return gson.fromJson<Cart>(dataString, Cart::class.java).items
    }

    internal class Cart(val items: List<Product>)

    internal class CartTypeAdapter : JsonSerializer<Cart>, JsonDeserializer<Cart> {

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Cart {
            val items = mutableListOf<Product>()
            val jsonArray = json.asJsonArray
            for (jsonObject in jsonArray) {
                val isPizza = jsonObject.asJsonObject.has("ingredients")
                if (isPizza) {
                    items.add(context.deserialize(jsonObject, Pizza::class.java))
                } else {
                    items.add(context.deserialize(jsonObject, Drink::class.java))
                }
            }
            return Cart(items)
        }

        override fun serialize(src: Cart, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
            return context.serialize(src.items)
        }
    }
}