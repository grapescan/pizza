package me.grapescan.nennopizza.data.catalog

import io.reactivex.Completable
import io.reactivex.Observable
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza

interface CatalogRepository {
    val pizzas: Observable<Result<List<Pizza>>>
    val drinks: Observable<Result<List<Drink>>>
    val ingredients: Observable<Result<List<Ingredient>>>
    val basePizzaPrice: Observable<Result<Double>>
    fun refresh(): Completable

    data class Result<out T>(val data: T?, val error: Throwable?)
}