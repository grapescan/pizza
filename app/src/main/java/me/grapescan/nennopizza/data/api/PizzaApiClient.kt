package me.grapescan.nennopizza.data.api

import io.reactivex.Completable
import io.reactivex.Single
import me.grapescan.nennopizza.BuildConfig
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Product
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

class PizzaApiClient : ApiClient {

    private val pizzaApiService = buildService("https://api.myjson.com", PizzaApiService::class.java)
    private val checkoutApiService = buildService("http://httpbin.org", CheckoutApiService::class.java)

    override fun getPizzas(): Single<PizzasResponse> = pizzaApiService.getPizzas()

    override fun getDrinks(): Single<List<Drink>> = pizzaApiService.getDrinks()

    override fun getIngredients(): Single<List<Ingredient>> = pizzaApiService.getIngredients()

    override fun checkout(items: List<Product>): Completable = checkoutApiService.checkout(Order(items))

    private fun <T> buildService(baseUrl: String, serviceClass: Class<T>): T = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = if (BuildConfig.DEBUG) {
                            HttpLoggingInterceptor.Level.BODY
                        } else {
                            HttpLoggingInterceptor.Level.NONE
                        }
                    })
                    .build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(serviceClass)

    interface PizzaApiService {

        @GET("/bins/ozt3z")
        fun getIngredients(): Single<List<Ingredient>>

        @GET("/bins/150da7")
        fun getDrinks(): Single<List<Drink>>

        @GET("/bins/dokm7")
        fun getPizzas(): Single<PizzasResponse>
    }

    interface CheckoutApiService {

        @POST("/post")
        fun checkout(@Body items: Order): Completable
    }
}