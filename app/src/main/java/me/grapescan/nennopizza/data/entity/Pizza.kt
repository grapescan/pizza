package me.grapescan.nennopizza.data.entity

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable

@SuppressLint("ParcelCreator")
data class Pizza(
        override val name: String,
        val imageUrl: String?,
        val basePrice: Double,
        val ingredients: List<Ingredient>
) : Product, AutoParcelable {
    override val price: Double
        get() = basePrice + ingredients.map { it.price }.sum()

    val description: String
        get() = ingredients.map { it.name }.joinToString(", ")
}