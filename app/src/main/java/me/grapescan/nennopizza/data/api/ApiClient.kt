package me.grapescan.nennopizza.data.api

import io.reactivex.Completable
import io.reactivex.Single
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Product

interface ApiClient {
    fun getPizzas(): Single<PizzasResponse>
    fun getDrinks(): Single<List<Drink>>
    fun getIngredients(): Single<List<Ingredient>>
    fun checkout(items: List<Product>): Completable
}