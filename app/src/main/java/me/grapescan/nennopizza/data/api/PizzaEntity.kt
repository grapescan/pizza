package me.grapescan.nennopizza.data.api

import com.google.gson.annotations.SerializedName

data class PizzaEntity(
        @SerializedName("name") val name: String,
        @SerializedName("ingredients") val ingredientIds: List<Long>,
        @SerializedName("imageUrl") val imageUrl: String? = null
)