package me.grapescan.nennopizza.data.api

import com.google.gson.annotations.SerializedName
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.data.entity.Product

class Order(items: List<Product>) {
    @SerializedName("pizzas")
    val pizzas: List<PizzaEntity> = items.filterIsInstance<Pizza>()
            .map { PizzaEntity(it.name, it.ingredients.map { it.id }, it.imageUrl) }
    @SerializedName("drinks")
    val drinks: List<Long> = items.filterIsInstance<Drink>()
            .map { it.id }
}