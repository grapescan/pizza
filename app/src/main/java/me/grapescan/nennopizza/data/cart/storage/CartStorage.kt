package me.grapescan.nennopizza.data.cart.storage

import me.grapescan.nennopizza.data.entity.Product

interface CartStorage {
    fun save(items: List<Product>)
    fun load(): List<Product>
}