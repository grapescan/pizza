package me.grapescan.nennopizza.data.entity

interface Product {
    val name: String
    val price: Double
}