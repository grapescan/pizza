package me.grapescan.nennopizza.data.cart

import io.reactivex.Completable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.grapescan.nennopizza.data.api.ApiClient
import me.grapescan.nennopizza.data.cart.storage.CartStorage
import me.grapescan.nennopizza.data.entity.Product

class PersistentCartRepository(private val storage: CartStorage, private val apiClient: ApiClient) : CartRepository {

    override val items: Subject<List<Product>> = BehaviorSubject.createDefault(storage.load())

    override fun add(item: Product) {
        val data = ArrayList(storage.load())
        data.add(item)
        storage.save(data)
        items.onNext(data.toList())
    }

    override fun remove(item: Product) {
        val data = ArrayList(storage.load())
        data.remove(item)
        storage.save(data)
        items.onNext(data.toList())
    }

    override fun checkout(): Completable = apiClient.checkout(storage.load())
            .doOnComplete {
                storage.save(emptyList())
                items.onNext(emptyList())
            }
}