package me.grapescan.nennopizza.data.entity

import com.google.gson.annotations.SerializedName

data class Drink(
        @SerializedName("id") val id: Long,
        @SerializedName("name") override val name: String,
        @SerializedName("price") override val price: Double
) : Product