package me.grapescan.nennopizza.data.catalog

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.rxkotlin.Singles
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.grapescan.nennopizza.data.api.ApiClient
import me.grapescan.nennopizza.data.api.PizzasResponse
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza

class CatalogRepositoryImpl(private val apiClient: ApiClient) : CatalogRepository {

    override val pizzas: Subject<CatalogRepository.Result<List<Pizza>>> = BehaviorSubject.createDefault(CatalogRepository.Result(emptyList(), null))
    override val drinks: Subject<CatalogRepository.Result<List<Drink>>> = BehaviorSubject.createDefault(CatalogRepository.Result(emptyList(), null))
    override val ingredients: Subject<CatalogRepository.Result<List<Ingredient>>> = BehaviorSubject.createDefault(CatalogRepository.Result(emptyList(), null))
    override val basePizzaPrice: Subject<CatalogRepository.Result<Double>> = BehaviorSubject.createDefault(CatalogRepository.Result(0.0, null))

    override fun refresh(): Completable {
        val pizzaUpdate = Singles.zip(apiClient.getPizzas(), apiClient.getIngredients())
        { pizzas, ingredients -> PizzaListAndPrice(buildPizzaList(pizzas, ingredients), pizzas.basePrice) }
                .subscribeOn(Schedulers.io())
                .map { CatalogRepository.Result(it, null) }
                .onErrorResumeNext { error -> Single.just(CatalogRepository.Result(null, error)) }
                .doOnSuccess { result ->
                    if (result.data != null) {
                        pizzas.onNext(CatalogRepository.Result(result.data.pizza, null))
                        basePizzaPrice.onNext(CatalogRepository.Result(result.data.basePrice, null))
                    } else {
                        pizzas.onNext(CatalogRepository.Result(null, result.error))
                        basePizzaPrice.onNext(CatalogRepository.Result(null, result.error))
                    }
                }

        val drinksUpdate = apiClient.getDrinks()
                .subscribeOn(Schedulers.io())
                .map { CatalogRepository.Result(it, null) }
                .onErrorResumeNext { error -> Single.just(CatalogRepository.Result(null, error)) }
                .doOnSuccess(drinks::onNext)

        val ingredientsUpdate = apiClient.getIngredients()
                .subscribeOn(Schedulers.io())
                .map { it.sortedBy { it.name } }
                .map { CatalogRepository.Result(it, null) }
                .onErrorResumeNext { error -> Single.just(CatalogRepository.Result(null, error)) }
                .doOnSuccess(ingredients::onNext)

        return Completable.concat(listOf(pizzaUpdate.toCompletable(),
                drinksUpdate.toCompletable(),
                ingredientsUpdate.toCompletable()))
    }

    private fun buildPizzaList(pizzaResponse: PizzasResponse, ingredients: List<Ingredient>): List<Pizza> {
        return pizzaResponse.pizzas.map {
            Pizza(it.name, it.imageUrl ?: "", pizzaResponse.basePrice, it.ingredientIds.map { ingredientId ->
                ingredients.find { it.id == ingredientId }!!
            })
        }
    }

    data class PizzaListAndPrice(val pizza: List<Pizza>, val basePrice: Double)
}