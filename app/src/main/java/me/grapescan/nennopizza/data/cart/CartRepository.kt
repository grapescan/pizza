package me.grapescan.nennopizza.data.cart

import io.reactivex.Completable
import io.reactivex.Observable
import me.grapescan.nennopizza.data.entity.Product

interface CartRepository {

    val items: Observable<List<Product>>

    fun add(item: Product)
    fun remove(item: Product)
    fun checkout(): Completable
}