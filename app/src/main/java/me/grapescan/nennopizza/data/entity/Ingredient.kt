package me.grapescan.nennopizza.data.entity

import android.annotation.SuppressLint
import com.google.gson.annotations.SerializedName
import io.mironov.smuggler.AutoParcelable

@SuppressLint("ParcelCreator")
data class Ingredient(
        @SerializedName("id") val id: Long,
        @SerializedName("name") val name: String,
        @SerializedName("price") val price: Double
) : AutoParcelable