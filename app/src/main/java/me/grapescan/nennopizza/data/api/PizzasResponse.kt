package me.grapescan.nennopizza.data.api

import com.google.gson.annotations.SerializedName

data class PizzasResponse(
        @SerializedName("pizzas") val pizzas: List<PizzaEntity>,
        @SerializedName("basePrice") val basePrice: Double
)