package me.grapescan.nennopizza.ui.base

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_product.view.*
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.ui.format.PriceFormatter

class ProductAdapter<T : Product>(
        private val priceFormatter: PriceFormatter,
        private val actionListener: (T) -> Unit = {},
        private val actionIconResId: Int? = null
) : ListAdapter<T, ProductAdapter.ProductViewHolder<T>>(ProductDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder<T> {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view, priceFormatter, actionListener, actionIconResId)
    }

    override fun onBindViewHolder(holder: ProductViewHolder<T>, position: Int) {
        holder.bind(getItem(position))
    }

    class ProductViewHolder<in T : Product>(
            view: View,
            private val priceFormatter: PriceFormatter,
            private val actionListener: (T) -> Unit,
            private val actionIconResId: Int?
    ) : RecyclerView.ViewHolder(view) {

        private val title = itemView.productTitle
        private val price = itemView.productPrice
        private val actionIcon = itemView.productActionIcon

        fun bind(item: T) {
            title.text = item.name
            price.text = priceFormatter.format(item.price)
            if (actionIconResId != null) {
                actionIcon.setImageResource(actionIconResId)
                actionIcon.setOnClickListener { actionListener(item) }
                actionIcon.visibility = View.VISIBLE
            } else {
                actionIcon.visibility = View.GONE
            }
        }
    }

    class ProductDiffCallback<T : Product> : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
            return oldItem == newItem
        }
    }
}