package me.grapescan.nennopizza.ui.cart

import me.grapescan.nennopizza.data.entity.Product

interface CartView {
    fun showProductList(data: List<Product>)
    fun showError(error: Throwable)
    fun showEmptyCart()
    fun openOrderConfirmation()
    fun openDrinkList()
}