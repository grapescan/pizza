package me.grapescan.nennopizza.ui.cart

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.nennopizza.data.cart.CartRepository
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.ui.base.Presenter

class CartPresenter(
        private val cartRepository: CartRepository
) : Presenter<CartView>() {
    override fun attachView(viewParam: CartView) {
        super.attachView(viewParam)
        cartRepository.items
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { items ->
                    if (items.isEmpty()) {
                        view?.showEmptyCart()
                    } else {
                        view?.showProductList(items)
                    }
                }
                .disposeOnDetach()
    }

    fun onCheckoutClick() {
        cartRepository.checkout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.openOrderConfirmation()
                }, { error ->
                    view?.showError(error)
                })
    }

    fun onDrinksItemClick() {
        view?.openDrinkList()
    }

    fun onRemoveFromCartClick(item: Product) {
        cartRepository.remove(item)
    }
}