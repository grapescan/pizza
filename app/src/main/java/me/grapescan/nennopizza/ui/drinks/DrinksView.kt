package me.grapescan.nennopizza.ui.drinks

import me.grapescan.nennopizza.data.entity.Drink

interface DrinksView {
    fun showDrinksList(data: List<Drink>)
    fun showError(error: Throwable)
    fun showDrinkAdded()
}