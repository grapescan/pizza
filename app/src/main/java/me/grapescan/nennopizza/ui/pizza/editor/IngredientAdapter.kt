package me.grapescan.nennopizza.ui.pizza.editor

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_ingredient.view.*
import kotlinx.android.synthetic.main.item_pizza_header.view.*
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.ui.base.loadBackgroundFromResource
import me.grapescan.nennopizza.ui.base.loadFromResource
import me.grapescan.nennopizza.ui.base.loadFromUrl
import me.grapescan.nennopizza.ui.format.PriceFormatter

class IngredientAdapter(
        private val basePizza: Pizza,
        private val priceFormatter: PriceFormatter,
        private val actionListener: (Pizza) -> Unit
) : ListAdapter<Ingredient, IngredientAdapter.BaseViewHolder>(ProductDiffCallback()) {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_INGREDIENT = 1
    }

    val pizza: Pizza
        get() = buildPizza()

    private val state = mutableMapOf<Ingredient,Boolean>()

    override fun getItemCount(): Int {
        return super.getItemCount() + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            TYPE_HEADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pizza_header, parent, false)
                HeaderViewHolder(view, basePizza.imageUrl)
            }
            TYPE_INGREDIENT -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_ingredient, parent, false)
                IngredientViewHolder(view, priceFormatter, { actionListener(buildPizza()) })
            }
            else -> throw IllegalStateException("View type $viewType not supported")
        }
    }

    override fun submitList(list: List<Ingredient>) {
        list.forEach { state[it] = it in basePizza.ingredients }
        super.submitList(list)
    }

    override fun getItemViewType(position: Int): Int = if (position == 0) TYPE_HEADER else TYPE_INGREDIENT

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (position > 0) {
            (holder as IngredientViewHolder).bind(getItem(position - 1))
        }
    }

    private fun buildPizza(): Pizza {
        val checkedIngredients = state.keys.filter { state[it] == true }
        return basePizza.copy(ingredients = checkedIngredients)
    }

    abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view)

    inner class IngredientViewHolder(
            view: View,
            private val priceFormatter: PriceFormatter,
            private val itemStateListener: () -> Unit
    ) : BaseViewHolder(view) {

        private val title = itemView.ingredientTitle
        private val price = itemView.ingredientPrice
        private val checkbox = itemView.ingredientCheckbox

        fun bind(item: Ingredient) {
            title.text = item.name
            price.text = priceFormatter.format(item.price)
            checkbox.isChecked = state[item]!!
            checkbox.setOnClickListener { onClick(item) }
            itemView.setOnClickListener { onClick(item) }
        }

        private fun onClick(item: Ingredient) {
            state[item] = !state[item]!!
            checkbox.isChecked = state[item]!!
            itemStateListener()
        }
    }

    class HeaderViewHolder(view: View, imageUrl: String?) : BaseViewHolder(view) {
        init {
            itemView.pizzaPhoto.loadBackgroundFromResource(R.drawable.bg_wood)
            when (imageUrl) {
                "" -> view.pizzaPhoto.setImageDrawable(null)
                null -> view.pizzaPhoto.loadFromResource(R.drawable.custom)
                else -> view.pizzaPhoto.loadFromUrl(imageUrl)
            }
        }
    }

    class ProductDiffCallback : DiffUtil.ItemCallback<Ingredient>() {
        override fun areItemsTheSame(oldItem: Ingredient, newItem: Ingredient): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Ingredient, newItem: Ingredient): Boolean {
            return oldItem == newItem
        }
    }
}