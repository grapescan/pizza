package me.grapescan.nennopizza.ui.pizza.editor

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_pizza_editor.*
import me.grapescan.nennopizza.Injector
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza


class PizzaEditorActivity : AppCompatActivity(), PizzaEditorView {

    companion object {

        private const val EXTRA_PIZZA = "pizza"

        fun startEditor(activity: Activity, pizza: Pizza) {
            val intent = Intent(activity, PizzaEditorActivity::class.java)
            intent.putExtra(EXTRA_PIZZA, pizza)
            activity.startActivity(intent)
        }

        fun startConstructor(activity: Activity, basePrice: Double) {
            val customPizza = Pizza(
                    activity.resources.getString(R.string.custom_pizza_title),
                    null,
                    basePrice,
                    emptyList())
            val intent = Intent(activity, PizzaEditorActivity::class.java)
            intent.putExtra(EXTRA_PIZZA, customPizza)
            activity.startActivity(intent)
        }
    }

    private val presenter by lazy { Injector.pizzaEditorPresenter.apply { basePizza = intent.getParcelableExtra(EXTRA_PIZZA) } }
    private val priceFormatter by lazy { Injector.priceFormatter }
    private lateinit var listAdapter: IngredientAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pizza_editor)
        addToCartButton.setOnClickListener { presenter.onAddToCartClick(listAdapter.pizza) }
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun showContent(pizza: Pizza, ingredients: List<Ingredient>) {
        listAdapter = IngredientAdapter(pizza, Injector.priceFormatter, presenter::onPizzaUpdate)
        ingredientList.adapter = listAdapter
        listAdapter.submitList(ingredients)
        supportActionBar?.title = pizza.name
    }

    override fun showError(error: Throwable) {
        Toast.makeText(this, R.string.general_error, Toast.LENGTH_SHORT).show()
    }

    override fun showPrice(price: Double) {
        val checkoutString = SpannableString(resources.getString(R.string.pizza_editor_add_to_cart, priceFormatter.format(price)))
        checkoutString.setSpan(ImageSpan(applicationContext, R.drawable.ic_cart_small,
                ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        checkoutString.setSpan(StyleSpan(Typeface.ITALIC), checkoutString.indexOf("("), checkoutString.length, 0)
        addToCartButton.text = checkoutString
    }

    override fun showAddedToCartBanner() {
        pizzaAddedToCart.visibility = View.VISIBLE
        Handler().postDelayed({
            pizzaAddedToCart.visibility = View.GONE
        }, 3000)
    }
}