package me.grapescan.nennopizza.ui.confirm

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_confirmation.*
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.ui.pizza.list.PizzaListActivity

class ConfirmationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
        confirmationCloseButton.setOnClickListener {
            startActivity(Intent(this, PizzaListActivity::class.java)
                    .apply { flags = Intent.FLAG_ACTIVITY_CLEAR_TOP })
        }
    }
}