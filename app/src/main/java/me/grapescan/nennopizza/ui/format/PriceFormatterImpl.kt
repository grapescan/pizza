package me.grapescan.nennopizza.ui.format

import android.content.Context
import me.grapescan.nennopizza.R

class PriceFormatterImpl(context: Context) : PriceFormatter {

    private val res = context.resources

    override fun format(sum: Double): String {
        val priceTemplate = when {
            sum % 1 == 0.0 -> R.string.price_no_fraction
            sum * 10 % 1 == 0.0 -> R.string.price_one_digit_fraction
            else -> R.string.price_two_digit_fraction
        }
        return res.getString(priceTemplate, sum)
    }
}
