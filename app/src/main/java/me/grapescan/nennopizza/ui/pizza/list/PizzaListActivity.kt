package me.grapescan.nennopizza.ui.pizza.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.ListPreloader.PreloadModelProvider
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.util.ViewPreloadSizeProvider
import kotlinx.android.synthetic.main.activity_pizza_list.*
import kotlinx.android.synthetic.main.menu_item_badge.view.*
import me.grapescan.nennopizza.Injector
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.ui.cart.CartActivity
import me.grapescan.nennopizza.ui.pizza.editor.PizzaEditorActivity

class PizzaListActivity : AppCompatActivity(), PizzaListView {

    private val presenter by lazy { Injector.pizzaListPresenter }
    private val listAdapter = PizzaAdapter(presenter::onAddToCartClick, presenter::onPizzaClick)
    private val preloader by lazy {
        val sizeProvider = ViewPreloadSizeProvider<Pizza>()
        val modelProvider = PizzaPreloadProvider(this, listAdapter)
        RecyclerViewPreloader<Pizza>(this, modelProvider, sizeProvider, 10)
    }
    private var badgeRoot: View? = null
    private var badgeCounter: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pizza_list)
        pizzaList.adapter = listAdapter
        pizzaList.addOnScrollListener(preloader)
        errorButton.setOnClickListener { presenter.onRetryClick() }
        createPizzaButton.setOnClickListener { presenter.onCreateClick() }
        if (savedInstanceState != null) {
            presenter.onViewReady()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
        if (badgeRoot != null) {
            presenter.onViewReady()
        }
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.pizza_list_menu, menu)
        val badgeMenuItem = menu.findItem(R.id.item_badge)
        badgeRoot = badgeMenuItem.actionView.menu_badge_root
        badgeCounter = badgeMenuItem.actionView.menu_badge_counter
        val cartMenuItem = menu.findItem(R.id.item_cart)
        cartMenuItem.actionView.setOnClickListener { presenter.onCartActionClick() }
        presenter.onViewReady()
        return true
    }

    override fun showPizzaList(data: List<Pizza>) {
        listAdapter.submitList(data)
        pizzaList.visibility = VISIBLE
        errorIcon.visibility = GONE
        errorMessage.visibility = GONE
        errorButton.visibility = GONE
    }

    override fun showError(error: Throwable) {
        pizzaList.visibility = GONE
        errorIcon.visibility = VISIBLE
        errorMessage.visibility = VISIBLE
        errorButton.visibility = VISIBLE
    }

    override fun openCart() = startActivity(Intent(this, CartActivity::class.java))

    override fun openPizzaEditor(pizza: Pizza) = PizzaEditorActivity.startEditor(this, pizza)

    override fun openPizzaConstructor(basePrice: Double) = PizzaEditorActivity.startConstructor(this, basePrice)

    override fun setCartCounter(value: Int) {
        badgeCounter?.text = if (value > 0) value.toString() else ""
        badgeRoot?.background = if (value > 0) ContextCompat.getDrawable(this, R.drawable.badge_bg) else null
        badgeRoot?.visibility = if (value > 0) View.VISIBLE else View.GONE
    }

    private class PizzaPreloadProvider(private val activity: Activity, private val pizzaAdapter: PizzaAdapter) : PreloadModelProvider<Pizza> {
        override fun getPreloadItems(position: Int): List<Pizza> = listOf(pizzaAdapter.getPizza(position))

        override fun getPreloadRequestBuilder(item: Pizza): RequestBuilder<*> {
            return Glide.with(activity)
                    .load(item.imageUrl)
        }
    }
}
