package me.grapescan.nennopizza.ui.base

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition

fun ImageView.loadFromResource(resId: Int) {
    Glide.with(this)
            .load(resId)
            .into(this)
}

fun ImageView.loadBackgroundFromResource(resId: Int) {
    Glide.with(this)
            .load(resId)
            .into(object : SimpleTarget<Drawable>() {
                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                    this@loadBackgroundFromResource.background = resource
                }
            })
}

fun ImageView.loadFromUrl(url: String?) {
    Glide.with(this)
            .load(url)
            .into(this)
}

