package me.grapescan.nennopizza.ui.cart

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.Menu
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_cart.*
import me.grapescan.nennopizza.Injector
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.ui.base.ProductAdapter
import me.grapescan.nennopizza.ui.confirm.ConfirmationActivity
import me.grapescan.nennopizza.ui.drinks.DrinksActivity

class CartActivity : AppCompatActivity(), CartView {

    private val presenter by lazy { Injector.cartPresenter }
    private val priceFormatter by lazy { Injector.priceFormatter }
    private val listAdapter by lazy { ProductAdapter<Product>(priceFormatter, presenter::onRemoveFromCartClick, R.mipmap.delete) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        cartProductList.adapter = listAdapter
        addToCartButton.setOnClickListener { presenter.onCheckoutClick() }
    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.cart_menu, menu)
        menu.findItem(R.id.item_drinks).actionView.setOnClickListener { presenter.onDrinksItemClick() }
        return true
    }

    override fun showProductList(data: List<Product>) {
        emptyCartIcon.visibility = View.GONE
        emptyCartMessage.visibility = View.GONE
        cartProductList.visibility = View.VISIBLE
        listAdapter.submitList(data)
        if (data.isEmpty()) {
            addToCartButton.visibility = View.GONE
        } else {
            updateCheckoutButton(data.map { it.price }.sum())
            addToCartButton.visibility = View.VISIBLE
        }
    }

    override fun showEmptyCart() {
        emptyCartIcon.visibility = View.VISIBLE
        emptyCartMessage.visibility = View.VISIBLE
        cartProductList.visibility = View.GONE
        addToCartButton.visibility = View.GONE
    }

    override fun showError(error: Throwable) {
        Toast.makeText(this, R.string.general_error, Toast.LENGTH_SHORT).show()
    }

    override fun openOrderConfirmation() {
        startActivity(Intent(this, ConfirmationActivity::class.java))
    }

    override fun openDrinkList() {
        startActivity(Intent(this, DrinksActivity::class.java))
    }

    private fun updateCheckoutButton(sum: Double) {
        val checkoutString = SpannableString(resources.getString(R.string.cart_checkout, priceFormatter.format(sum)))
        checkoutString.setSpan(StyleSpan(Typeface.ITALIC), checkoutString.indexOf("("), checkoutString.length, 0)
        addToCartButton.text = checkoutString
    }

}
