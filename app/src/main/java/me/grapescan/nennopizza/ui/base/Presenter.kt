package me.grapescan.nennopizza.ui.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Presenter<T> {

    protected var view: T? = null
    private var compositeDisposable = CompositeDisposable()

    open fun attachView(viewParam: T) {
        this.view = viewParam
    }

    open fun detachView() {
        view = null
        disposeAll()
    }

    protected fun addSubscription(subscription: Disposable) {
        compositeDisposable.add(subscription)
    }

    protected fun removeSubscription(subscription: Disposable) {
        compositeDisposable.remove(subscription)
    }

    protected fun Disposable.disposeOnDetach(): Disposable {
        compositeDisposable.add(this)
        return this
    }

    protected fun disposeAll() {
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
    }
}