package me.grapescan.nennopizza.ui.drinks

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_drinks.*
import me.grapescan.nennopizza.Injector
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.ui.base.ProductAdapter

class DrinksActivity : AppCompatActivity(), DrinksView {

    private val presenter by lazy { Injector.drinksPresenter }
    private val listAdapter = ProductAdapter<Drink>(Injector.priceFormatter, presenter::onAddToCartClick, R.drawable.ic_plus)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drinks)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        drinkList.adapter = listAdapter
    }

    override fun onResume() {
        super.onResume()
        drinkAddedToCart.visibility = View.GONE
        presenter.attachView(this)
    }

    override fun onPause() {
        presenter.detachView()
        super.onPause()
    }

    override fun showDrinksList(data: List<Drink>) {
        listAdapter.submitList(data)
    }

    override fun showError(error: Throwable) {
        Toast.makeText(this, R.string.general_error, Toast.LENGTH_SHORT).show()
    }

    override fun showDrinkAdded() {
        drinkAddedToCart.visibility = View.VISIBLE
        Handler().postDelayed({
            drinkAddedToCart.visibility = View.GONE
        }, 3000)
    }
}