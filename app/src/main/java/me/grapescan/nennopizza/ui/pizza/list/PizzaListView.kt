package me.grapescan.nennopizza.ui.pizza.list

import me.grapescan.nennopizza.data.entity.Pizza

interface PizzaListView {
    fun showPizzaList(data: List<Pizza>)
    fun showError(error: Throwable)
    fun openCart()
    fun openPizzaEditor(pizza: Pizza)
    fun openPizzaConstructor(basePrice: Double)
    fun setCartCounter(value: Int)
}