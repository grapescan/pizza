package me.grapescan.nennopizza.ui.pizza.editor

import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza

interface PizzaEditorView {
    fun showContent(pizza: Pizza, ingredients: List<Ingredient>)
    fun showError(error: Throwable)
    fun showPrice(price: Double)
    fun showAddedToCartBanner()
}