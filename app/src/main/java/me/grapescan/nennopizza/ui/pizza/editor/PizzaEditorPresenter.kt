package me.grapescan.nennopizza.ui.pizza.editor

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.nennopizza.data.cart.CartRepository
import me.grapescan.nennopizza.data.catalog.CatalogRepository
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.ui.base.Presenter

class PizzaEditorPresenter(
        private val catalogRepository: CatalogRepository,
        private val cartRepository: CartRepository
) : Presenter<PizzaEditorView>() {

    lateinit var basePizza: Pizza

    override fun attachView(viewParam: PizzaEditorView) {
        super.attachView(viewParam)

        catalogRepository.ingredients
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ (ingredients, error) ->
                    if (error != null) {
                        view?.showError(error)
                    } else if (ingredients != null) {
                        view?.showContent(basePizza, ingredients)
                        view?.showPrice(basePizza.price)
                    }
                }).disposeOnDetach()
    }

    fun onPizzaUpdate(pizza: Pizza) {
        view?.showPrice(pizza.price)
    }

    fun onAddToCartClick(pizza: Pizza) {
        cartRepository.add(pizza)
        view?.showAddedToCartBanner()
    }
}