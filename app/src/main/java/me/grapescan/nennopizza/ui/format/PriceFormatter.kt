package me.grapescan.nennopizza.ui.format

interface PriceFormatter {
    fun format(sum: Double): String
}