package me.grapescan.nennopizza.ui.drinks

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.nennopizza.data.cart.CartRepository
import me.grapescan.nennopizza.data.catalog.CatalogRepository
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.ui.base.Presenter

class DrinksPresenter(
        private val catalogRepository: CatalogRepository,
        private val cartRepository: CartRepository
) : Presenter<DrinksView>() {

    override fun attachView(viewParam: DrinksView) {
        super.attachView(viewParam)
        catalogRepository.drinks
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    if (result.error != null) {
                        view?.showError(result.error)
                    } else if (result.data != null) {
                        view?.showDrinksList(result.data)
                    }
                }).disposeOnDetach()
    }

    fun onAddToCartClick(drink: Drink) {
        cartRepository.add(drink)
        view?.showDrinkAdded()
    }
}