package me.grapescan.nennopizza.ui.pizza.list

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.grapescan.nennopizza.data.cart.CartRepository
import me.grapescan.nennopizza.data.catalog.CatalogRepository
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.ui.base.Presenter

class PizzaListPresenter(
        private val catalogRepository: CatalogRepository,
        private val cartRepository: CartRepository
) : Presenter<PizzaListView>() {

    fun onViewReady() {
        catalogRepository.pizzas
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ (pizzas, error) ->
                    if (error != null) {
                        view?.showError(error)
                    } else if (pizzas != null) {
                        view?.showPizzaList(pizzas)
                    }
                }).disposeOnDetach()
        catalogRepository.refresh().subscribe()
        cartRepository.items
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { view?.setCartCounter(it.size) }
                .disposeOnDetach()
    }

    fun onCartActionClick() {
        view?.openCart()
    }

    fun onAddToCartClick(pizza: Pizza) {
        cartRepository.add(pizza)
    }

    fun onPizzaClick(pizza: Pizza) {
        view?.openPizzaEditor(pizza)
    }

    fun onRetryClick() {
        catalogRepository.refresh()
    }

    fun onCreateClick() {
        catalogRepository.basePizzaPrice
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { (basePrice, error) ->
                    if (error != null) {
                        view?.showError(error)
                    } else if (basePrice != null) {
                        view?.openPizzaConstructor(basePrice)
                    }
                }
                .disposeOnDetach()
    }
}