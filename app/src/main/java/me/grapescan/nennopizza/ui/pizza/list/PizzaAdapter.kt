package me.grapescan.nennopizza.ui.pizza.list

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_pizza.view.*
import me.grapescan.nennopizza.Injector
import me.grapescan.nennopizza.R
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.ui.base.loadBackgroundFromResource
import me.grapescan.nennopizza.ui.base.loadFromUrl

typealias PizzaClickListener = (Pizza) -> Unit

class PizzaAdapter(
        private val actionListener: PizzaClickListener,
        private val clickListener: PizzaClickListener
) : ListAdapter<Pizza, PizzaAdapter.PizzaViewHolder>(PizzaDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PizzaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pizza, parent, false)
        return PizzaViewHolder(view, actionListener, clickListener)
    }

    override fun onBindViewHolder(holder: PizzaViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun getPizza(position: Int): Pizza = getItem(position)

    class PizzaViewHolder(view: View,
                          private val actionListener: PizzaClickListener,
                          private val clickListener: PizzaClickListener
    ) : RecyclerView.ViewHolder(view) {

        private val priceFormatter by lazy { Injector.priceFormatter }
        private val title = itemView.pizzaTitle
        private val description = itemView.pizzaDescription
        private val image = itemView.pizzaPhoto
        private val addToCartButton = itemView.pizzaAddToCartButton

        init {
            image.loadBackgroundFromResource(R.drawable.bg_wood)
        }

        fun bind(item: Pizza) {
            title.text = item.name
            description.text = item.description
            addToCartButton.text = priceFormatter.format(item.price)
            addToCartButton.setOnClickListener { actionListener(item) }
            itemView.setOnClickListener { clickListener(item) }
            image.loadFromUrl(item.imageUrl)
        }
    }

    class PizzaDiffCallback : DiffUtil.ItemCallback<Pizza>() {
        override fun areItemsTheSame(oldItem: Pizza, newItem: Pizza): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Pizza, newItem: Pizza): Boolean {
            return oldItem == newItem
        }
    }
}