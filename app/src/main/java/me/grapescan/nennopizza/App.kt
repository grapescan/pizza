package me.grapescan.nennopizza

import android.app.Application

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.context = this
    }
}