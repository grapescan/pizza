package me.grapescan.nennopizza

import android.annotation.SuppressLint
import android.content.Context
import me.grapescan.nennopizza.data.api.PizzaApiClient
import me.grapescan.nennopizza.data.cart.PersistentCartRepository
import me.grapescan.nennopizza.data.cart.storage.PrefCartStorage
import me.grapescan.nennopizza.data.catalog.CatalogRepositoryImpl
import me.grapescan.nennopizza.ui.cart.CartPresenter
import me.grapescan.nennopizza.ui.drinks.DrinksPresenter
import me.grapescan.nennopizza.ui.format.PriceFormatterImpl
import me.grapescan.nennopizza.ui.pizza.editor.PizzaEditorPresenter
import me.grapescan.nennopizza.ui.pizza.list.PizzaListPresenter

@SuppressLint("StaticFieldLeak")
object Injector {
    lateinit var context: Context
    private val apiClient by lazy { PizzaApiClient() }
    private val catalogRepository by lazy { CatalogRepositoryImpl(apiClient) }
    private val cartStorage by lazy { PrefCartStorage(context) }
    private val cartRepository by lazy { PersistentCartRepository(cartStorage, apiClient) }
    val priceFormatter by lazy { PriceFormatterImpl(context) }
    val pizzaListPresenter by lazy { PizzaListPresenter(catalogRepository, cartRepository) }
    val pizzaEditorPresenter by lazy { PizzaEditorPresenter(catalogRepository, cartRepository) }
    val cartPresenter by lazy { CartPresenter(cartRepository) }
    val drinksPresenter by lazy { DrinksPresenter(catalogRepository, cartRepository) }
}