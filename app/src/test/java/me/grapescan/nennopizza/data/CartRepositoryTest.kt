package me.grapescan.nennopizza.data

import io.reactivex.observers.TestObserver
import me.grapescan.nennopizza.TestData
import me.grapescan.nennopizza.TestData.margherita
import me.grapescan.nennopizza.TestData.water
import me.grapescan.nennopizza.data.cart.CartRepository
import me.grapescan.nennopizza.data.cart.PersistentCartRepository
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.stubs.FakeStorage
import me.grapescan.nennopizza.stubs.api.CheckoutApiClient
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CartRepositoryTest {

    private lateinit var storage: FakeStorage
    private lateinit var apiClient: CheckoutApiClient
    private lateinit var cart: CartRepository

    @Before
    fun setUp() {
        apiClient = CheckoutApiClient()
        storage = FakeStorage()
        cart = PersistentCartRepository(storage, apiClient)
    }

    @Test
    fun testInitialValueIsEmpty() {
        val productObserver = TestObserver<List<Product>>()
        cart.items.subscribe(productObserver)
        productObserver.assertValueCount(1)
        Assert.assertTrue(productObserver.values()[0].isEmpty())
    }

    @Test
    fun testItemAdding() {
        val productObserver = TestObserver<List<Product>>()
        cart.items.subscribe(productObserver)
        cart.add(margherita)
        productObserver.assertValueCount(2)
        Assert.assertTrue(productObserver.values().last().size == 1)
    }

    @Test
    fun testItemRemoving() {
        val productObserver = TestObserver<List<Product>>()
        cart.items.subscribe(productObserver)
        cart.add(margherita)
        cart.add(margherita)
        Assert.assertTrue(productObserver.values().last().size == 2)
        cart.remove(margherita)
        Assert.assertTrue(productObserver.values().last().size == 1)
    }

    @Test
    fun testConsistency() {
        val firstObserver = TestObserver<List<Product>>()
        val secondObserver = TestObserver<List<Product>>()
        cart.items.subscribe(firstObserver)
        cart.add(margherita)
        Assert.assertTrue(firstObserver.values().last().size == 1)
        cart.items.subscribe(secondObserver)
        Assert.assertTrue(secondObserver.values().last().size == 1)
    }

    @Test
    fun testPersistence() {
        val storage = FakeStorage()
        val cart1 = PersistentCartRepository(storage, apiClient)
        cart1.add(TestData.margherita)
        cart1.add(TestData.water)
        val cart2 = PersistentCartRepository(storage, apiClient)
        val cartObserver = TestObserver<List<Product>>()
        cart2.items.subscribe(cartObserver)
        Assert.assertTrue(cartObserver.values().last().size == 2)
    }

    @Test
    fun testCheckoutApiCall() {
        cart.add(margherita)
        cart.add(water)
        Assert.assertTrue(apiClient.checkoutsCount == 0)
        cart.checkout().blockingGet()
        Assert.assertTrue(apiClient.checkoutsCount == 1)
        Assert.assertTrue(apiClient.lastOrderSize == 2)
    }

    @Test
    fun testCartIsEmptyAfterCheckout() {
        val productObserver = TestObserver<List<Product>>()
        cart.items.subscribe(productObserver)
        cart.add(water)
        productObserver.values().last().isNotEmpty()
        cart.checkout().blockingGet()
        productObserver.values().last().isEmpty()
    }
}