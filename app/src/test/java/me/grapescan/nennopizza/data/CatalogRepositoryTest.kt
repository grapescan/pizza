package me.grapescan.nennopizza.data

import io.reactivex.observers.TestObserver
import me.grapescan.nennopizza.data.catalog.CatalogRepository
import me.grapescan.nennopizza.data.catalog.CatalogRepositoryImpl
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.stubs.api.FailingApiClient
import me.grapescan.nennopizza.stubs.api.LoadingApiClient
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

class CatalogRepositoryTest {

    private lateinit var apiClient: LoadingApiClient
    private lateinit var catalog: CatalogRepository

    @Before
    fun setUp() {
        apiClient = LoadingApiClient()
        catalog = CatalogRepositoryImpl(apiClient)
    }

    @Test
    fun testInitialValueIsEmpty() {
        val pizzaObserver = TestObserver<CatalogRepository.Result<List<Pizza>>>()
        catalog.pizzas.subscribe(pizzaObserver)
        pizzaObserver.assertValueCount(1)
        Assert.assertTrue(pizzaObserver.values()[0].data?.isEmpty() ?: false)
    }

    @Test
    fun testErrorHandling() {
        catalog = CatalogRepositoryImpl(FailingApiClient())
        val pizzaObserver = TestObserver<CatalogRepository.Result<List<Pizza>>>()
        catalog.pizzas.subscribe(pizzaObserver)
        catalog.refresh().blockingGet()
        Assert.assertTrue(pizzaObserver.values()[1].error is IOException)
    }

    @Test
    fun testRefresh() {
        val pizzaObserver = TestObserver<CatalogRepository.Result<List<Pizza>>>()
        catalog.pizzas.subscribe(pizzaObserver)
        catalog.refresh().blockingGet()
        pizzaObserver.assertValueCount(2)
        Assert.assertTrue(apiClient.pizzaUpdates == 1)
    }

    @Test
    fun testDataIntegrity() {
        val pizzaObserver = TestObserver<CatalogRepository.Result<List<Pizza>>>()
        catalog.pizzas.subscribe(pizzaObserver)
        catalog.refresh().blockingGet()
        Assert.assertEquals(pizzaObserver.values()[1].data?.get(0)?.name, "Margherita")
    }

    @Test
    fun testCaching() {
        apiClient = LoadingApiClient()
        catalog = CatalogRepositoryImpl(apiClient)
        catalog.refresh().blockingGet()
        val firstObserver = TestObserver<CatalogRepository.Result<List<Pizza>>>()
        catalog.pizzas.subscribe(firstObserver)
        val firstValue = firstObserver.values().last()
        val secondObserver = TestObserver<CatalogRepository.Result<List<Pizza>>>()
        catalog.pizzas.subscribe(secondObserver)
        val secondValue = secondObserver.values().last()
        Assert.assertEquals(firstValue, secondValue)
        Assert.assertTrue(apiClient.pizzaUpdates == 1)
    }
}