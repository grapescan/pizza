package me.grapescan.nennopizza.stubs.catalog

import io.reactivex.Observable
import me.grapescan.nennopizza.data.catalog.CatalogRepository
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza
import java.io.IOException

class FailingCatalogRepository : FakeCatalogRepository() {

    override val pizzas: Observable<CatalogRepository.Result<List<Pizza>>> = Observable.just(CatalogRepository.Result(null, IOException()))
    override val drinks: Observable<CatalogRepository.Result<List<Drink>>> = Observable.just(CatalogRepository.Result(null, IOException()))
    override val ingredients: Observable<CatalogRepository.Result<List<Ingredient>>> = Observable.just(CatalogRepository.Result(null, IOException()))
}