package me.grapescan.nennopizza.stubs.api

import io.reactivex.Completable
import io.reactivex.Single
import me.grapescan.nennopizza.data.api.ApiClient
import me.grapescan.nennopizza.data.api.PizzasResponse
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Product

class CheckoutApiClient : ApiClient {

    var checkoutsCount = 0
    var lastOrderSize = -1

    override fun getPizzas(): Single<PizzasResponse> = Single.never()

    override fun getDrinks(): Single<List<Drink>> = Single.never()

    override fun getIngredients(): Single<List<Ingredient>> = Single.never()

    override fun checkout(items: List<Product>): Completable {
        lastOrderSize = items.size
        checkoutsCount++
        return Completable.complete()
    }
}