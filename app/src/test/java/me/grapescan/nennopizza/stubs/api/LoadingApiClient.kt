package me.grapescan.nennopizza.stubs.api

import io.reactivex.Completable
import io.reactivex.Single
import me.grapescan.nennopizza.TestData
import me.grapescan.nennopizza.data.api.ApiClient
import me.grapescan.nennopizza.data.api.PizzaEntity
import me.grapescan.nennopizza.data.api.PizzasResponse
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Product

class LoadingApiClient : ApiClient {

    var pizzaUpdates = 0
    var drinkUpdates = 0
    var ingredientUpdates = 0

    override fun getPizzas(): Single<PizzasResponse> {
        pizzaUpdates++
        return Single.just(PizzasResponse(listOf(PizzaEntity("Margherita", listOf(0, 1), "http://some-url.com")), 1.0))
    }

    override fun getDrinks(): Single<List<Drink>> {
        drinkUpdates++
        return Single.just(listOf(TestData.water))
    }

    override fun getIngredients(): Single<List<Ingredient>> {
        ingredientUpdates++
        return Single.just(listOf(TestData.tomatoSauce, TestData.mozzarella))
    }

    override fun checkout(items: List<Product>): Completable = Completable.never()
}