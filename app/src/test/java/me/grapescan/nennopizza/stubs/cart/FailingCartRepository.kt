package me.grapescan.nennopizza.stubs.cart

import io.reactivex.Completable
import java.io.IOException

class FailingCartRepository : FakeCartRepository() {

    override fun checkout(): Completable = Completable.error(IOException())
}