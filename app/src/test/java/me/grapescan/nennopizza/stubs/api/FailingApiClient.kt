package me.grapescan.nennopizza.stubs.api

import io.reactivex.Completable
import io.reactivex.Single
import me.grapescan.nennopizza.data.api.ApiClient
import me.grapescan.nennopizza.data.api.PizzasResponse
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Product
import java.io.IOException

class FailingApiClient : ApiClient {

    override fun getPizzas(): Single<PizzasResponse> = Single.error(IOException())

    override fun getDrinks(): Single<List<Drink>> = Single.error(IOException())

    override fun getIngredients(): Single<List<Ingredient>> = Single.error(IOException())

    override fun checkout(items: List<Product>): Completable = Completable.never()
}