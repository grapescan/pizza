package me.grapescan.nennopizza.stubs

import me.grapescan.nennopizza.data.cart.storage.CartStorage
import me.grapescan.nennopizza.data.entity.Product

class FakeStorage : CartStorage {

    var data = emptyList<Product>()

    override fun save(items: List<Product>) {
        data = items
    }

    override fun load(): List<Product> = data
}