package me.grapescan.nennopizza.stubs.catalog

import io.reactivex.Completable
import io.reactivex.Observable
import me.grapescan.nennopizza.TestData
import me.grapescan.nennopizza.data.catalog.CatalogRepository
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza

open class FakeCatalogRepository : CatalogRepository {

    var refreshCount = 0

    override val pizzas: Observable<CatalogRepository.Result<List<Pizza>>> = Observable.just(CatalogRepository.Result<List<Pizza>>(listOf(TestData.margherita), null))
    override val drinks: Observable<CatalogRepository.Result<List<Drink>>> = Observable.just(CatalogRepository.Result<List<Drink>>(listOf(TestData.water), null))
    override val ingredients: Observable<CatalogRepository.Result<List<Ingredient>>> = Observable.just(CatalogRepository.Result<List<Ingredient>>(listOf(TestData.tomatoSauce, TestData.mozzarella), null))
    override val basePizzaPrice: Observable<CatalogRepository.Result<Double>> = Observable.just(CatalogRepository.Result<Double>(1.0, null))

    override fun refresh(): Completable {
        refreshCount++
        return Completable.complete()
    }
}