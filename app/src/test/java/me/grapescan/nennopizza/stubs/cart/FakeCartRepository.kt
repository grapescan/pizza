package me.grapescan.nennopizza.stubs.cart

import io.reactivex.Completable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.grapescan.nennopizza.data.cart.CartRepository
import me.grapescan.nennopizza.data.entity.Product

open class FakeCartRepository : CartRepository {

    var isCheckoutSucceed: Boolean? = null

    private val data: MutableList<Product> = mutableListOf()
    override val items: Subject<List<Product>> = BehaviorSubject.createDefault(emptyList())

    override fun add(item: Product) {
        data.add(item)
        items.onNext(data.toList())
    }

    override fun remove(item: Product) {
        data.remove(item)
        items.onNext(data)
    }

    override fun checkout(): Completable {
        return Completable.fromAction { isCheckoutSucceed = true }
    }
}