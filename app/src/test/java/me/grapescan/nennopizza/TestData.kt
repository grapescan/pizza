package me.grapescan.nennopizza

import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza

object TestData {
    val mozzarella = Ingredient(0, "Mozarella", 1.0)
    val tomatoSauce = Ingredient(1, "Tomato Sauce", 0.8)
    val margherita = Pizza( "Margherita", null, 2.0, listOf(mozzarella, tomatoSauce))
    val water = Drink(0, "Water", 0.5)
}