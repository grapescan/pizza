package me.grapescan.nennopizza.ui

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert
import me.grapescan.nennopizza.TestData
import me.grapescan.nennopizza.data.entity.Ingredient
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.stubs.cart.FakeCartRepository
import me.grapescan.nennopizza.stubs.catalog.FailingCatalogRepository
import me.grapescan.nennopizza.stubs.catalog.FakeCatalogRepository
import me.grapescan.nennopizza.ui.pizza.editor.PizzaEditorPresenter
import me.grapescan.nennopizza.ui.pizza.editor.PizzaEditorView
import org.junit.Before
import org.junit.Test
import java.io.IOException

class PizzaEditorPresenterTest {

    private val delayMillis = 200L

    private lateinit var catalog: FakeCatalogRepository
    private lateinit var cart: FakeCartRepository
    private lateinit var presenter: PizzaEditorPresenter
    private lateinit var view: FakePizzaEditorView

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        view = FakePizzaEditorView()
        catalog = FakeCatalogRepository()
        cart = FakeCartRepository()
        presenter = PizzaEditorPresenter(catalog, cart)
    }

    @Test
    fun testPizzaLoading() {
        presenter.basePizza = TestData.margherita
        presenter.attachView(view)
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.pizza == TestData.margherita)
    }

    @Test
    fun testErrorHandling() {
        presenter = PizzaEditorPresenter(FailingCatalogRepository(), cart)
        presenter.basePizza = TestData.margherita
        presenter.attachView(view)
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.pizza == null && view.ingredients == null)
        Assert.assertTrue(view.error is IOException)
    }

    @Test
    fun testPizzaUpdates() {
        val pizza1 = Pizza("Pizza", null, 1.0, listOf(TestData.mozzarella))
        val pizza2 = Pizza("Pizza", null, 1.0, listOf(TestData.mozzarella, TestData.tomatoSauce))
        presenter.basePizza = pizza1
        presenter.attachView(view)
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.price == pizza1.price)
        presenter.onPizzaUpdate(pizza2)
        Assert.assertTrue(view.price == pizza2.price)
    }

    @Test
    fun testAddToCart() {
        val cartObserver = TestObserver<List<Product>>()
        cart.items.subscribe(cartObserver)
        presenter.basePizza = TestData.margherita
        presenter.attachView(view)
        Assert.assertTrue(cartObserver.values().last().isEmpty())
        presenter.onAddToCartClick(TestData.margherita)
        Assert.assertTrue(cartObserver.values().last().isNotEmpty())
    }

    @Test
    fun testAddBanner() {
        presenter.basePizza = TestData.margherita
        presenter.attachView(view)
        presenter.onAddToCartClick(TestData.margherita)
        Assert.assertTrue(view.isBannerVisible)
    }

    class FakePizzaEditorView : PizzaEditorView {

        var pizza: Pizza? = null
        var ingredients: List<Ingredient>? = null
        var error: Throwable? = null
        var price: Double? = null
        var isBannerVisible = false

        override fun showContent(pizza: Pizza, ingredients: List<Ingredient>) {
            this.pizza = pizza
            this.ingredients = ingredients
        }

        override fun showError(error: Throwable) {
            this.error = error
        }

        override fun showPrice(price: Double) {
            this.price = price
        }

        override fun showAddedToCartBanner() {
            isBannerVisible = true
        }

    }
}