package me.grapescan.nennopizza.ui

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert
import me.grapescan.nennopizza.TestData
import me.grapescan.nennopizza.data.entity.Drink
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.stubs.cart.FakeCartRepository
import me.grapescan.nennopizza.stubs.catalog.FailingCatalogRepository
import me.grapescan.nennopizza.stubs.catalog.FakeCatalogRepository
import me.grapescan.nennopizza.ui.drinks.DrinksPresenter
import me.grapescan.nennopizza.ui.drinks.DrinksView
import org.junit.Before
import org.junit.Test
import java.io.IOException

class DrinksPresenterTest {
    private val delayMillis = 200L

    private lateinit var catalog: FakeCatalogRepository
    private lateinit var cart: FakeCartRepository
    private lateinit var presenter: DrinksPresenter
    private lateinit var view: FakeDrinksView

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        view = FakeDrinksView()
        catalog = FakeCatalogRepository()
        cart = FakeCartRepository()
        presenter = DrinksPresenter(catalog, cart)
    }

    @Test
    fun testInitialListLoading() {
        presenter.attachView(view)
        Assert.assertTrue(view.data == null)
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.isListVisible)
        Assert.assertTrue(view.data != null && view.data?.size ?: -1 == 1)
    }

    @Test
    fun testErrorHandling() {
        presenter = DrinksPresenter(FailingCatalogRepository(), cart)
        presenter.attachView(view)
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.data == null)
        Assert.assertTrue(view.error is IOException)
    }

    @Test
    fun testAddToCart() {
        val cartObserver = TestObserver<List<Product>>()
        cart.items.subscribe(cartObserver)
        presenter.attachView(view)
        Assert.assertTrue(cartObserver.values().last().isEmpty())
        presenter.onAddToCartClick(TestData.water)
        Assert.assertTrue(cartObserver.values().last().isNotEmpty())
    }

    @Test
    fun testAddBanner() {
        presenter.attachView(view)
        presenter.onAddToCartClick(TestData.water)
        Assert.assertTrue(view.isAddBannerVisible)
    }

    class FakeDrinksView : DrinksView {

        var data: List<Drink>? = null
        var error: Throwable? = null
        var isListVisible = false
        var isErrorVisible = false
        var isAddBannerVisible = false

        override fun showDrinksList(data: List<Drink>) {
            this.data = data
            isListVisible = true
        }

        override fun showError(error: Throwable) {
            this.error = error
            isErrorVisible = true
        }

        override fun showDrinkAdded() {
            isAddBannerVisible = true
        }

    }
}