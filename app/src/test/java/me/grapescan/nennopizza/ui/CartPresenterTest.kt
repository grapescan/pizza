package me.grapescan.nennopizza.ui

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert
import me.grapescan.nennopizza.TestData
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.stubs.cart.FailingCartRepository
import me.grapescan.nennopizza.stubs.cart.FakeCartRepository
import me.grapescan.nennopizza.ui.cart.CartPresenter
import me.grapescan.nennopizza.ui.cart.CartView
import org.junit.Before
import org.junit.Test

class CartPresenterTest {

    private val delayMillis = 200L

    private lateinit var cart: FakeCartRepository
    private lateinit var presenter: CartPresenter
    private lateinit var view: FakeCartView

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        view = FakeCartView()
        cart = FakeCartRepository()
        presenter = CartPresenter(cart)
    }

    @Test
    fun testInitialStateIsEmpty() {
        presenter.attachView(view)
        Assert.assertTrue(view.state == FakeCartView.State.EMPTY)
    }

    @Test
    fun testNonEmptyState() {
        cart.add(TestData.margherita)
        presenter.attachView(view)
        Assert.assertTrue(view.state == FakeCartView.State.LIST)
    }

    @Test
    fun testErrorWhileCheckout() {
        presenter = CartPresenter(FailingCartRepository())
        presenter.attachView(view)
        presenter.onCheckoutClick()
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.state == FakeCartView.State.ERROR)
    }

    @Test
    fun testCheckout() {
        presenter.attachView(view)
        Assert.assertTrue(cart.isCheckoutSucceed == null)
        presenter.onCheckoutClick()
        Thread.sleep(delayMillis)
        Assert.assertTrue(cart.isCheckoutSucceed == true)
    }

    @Test
    fun testOpenDrinks() {
        presenter.attachView(view)
        Assert.assertFalse(view.isDrinksOpened)
        presenter.onDrinksItemClick()
        Assert.assertTrue(view.isDrinksOpened)
    }

    @Test
    fun testItemRemoving() {
        cart.add(TestData.margherita)
        cart.add(TestData.water)
        presenter.attachView(view)
        Assert.assertTrue(view.data?.size == 2)
        presenter.onRemoveFromCartClick(TestData.margherita)
        Assert.assertTrue(view.data?.size == 1)
    }

    class FakeCartView : CartView {

        var state = State.UNKNOWN
        var data: List<Product>? = null
        var isConfirmationOpened = false
        var isDrinksOpened = false

        override fun showProductList(data: List<Product>) {
            state = State.LIST
            this.data = data

        }

        override fun showError(error: Throwable) {
            state = State.ERROR
        }

        override fun showEmptyCart() {
            state = State.EMPTY
        }

        override fun openOrderConfirmation() {
            isConfirmationOpened = true
        }

        override fun openDrinkList() {
            isDrinksOpened = true
        }

        enum class State {
            UNKNOWN, LIST, ERROR, EMPTY
        }

    }
}