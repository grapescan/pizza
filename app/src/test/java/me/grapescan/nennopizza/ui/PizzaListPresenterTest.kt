package me.grapescan.nennopizza.ui

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert
import me.grapescan.nennopizza.TestData.margherita
import me.grapescan.nennopizza.data.entity.Pizza
import me.grapescan.nennopizza.data.entity.Product
import me.grapescan.nennopizza.stubs.cart.FakeCartRepository
import me.grapescan.nennopizza.stubs.catalog.FailingCatalogRepository
import me.grapescan.nennopizza.stubs.catalog.FakeCatalogRepository
import me.grapescan.nennopizza.ui.pizza.list.PizzaListPresenter
import me.grapescan.nennopizza.ui.pizza.list.PizzaListView
import org.junit.Before
import org.junit.Test
import java.io.IOException

class PizzaListPresenterTest {

    private val delayMillis = 200L

    private lateinit var catalog: FakeCatalogRepository
    private lateinit var cart: FakeCartRepository
    private lateinit var presenter: PizzaListPresenter
    private lateinit var view: FakePizzaListView

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        view = FakePizzaListView()
        catalog = FakeCatalogRepository()
        cart = FakeCartRepository()
        presenter = PizzaListPresenter(catalog, cart)
    }

    @Test
    fun testInitialListLoading() {
        presenter.attachView(view)
        Assert.assertTrue(view.data == null)
        presenter.onViewReady()
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.data != null && view.data?.size ?: -1 == 1)
    }

    @Test
    fun testErrorHandling() {
        presenter = PizzaListPresenter(FailingCatalogRepository(), cart)
        presenter.attachView(view)
        Assert.assertTrue(view.data == null)
        presenter.onViewReady()
        Thread.sleep(delayMillis)
        Assert.assertTrue(view.data == null)
        Assert.assertTrue(view.error is IOException)
    }

    @Test
    fun testTryAgainAfterError() {
        val failingCatalog = FailingCatalogRepository()
        presenter = PizzaListPresenter(failingCatalog, cart)
        presenter.attachView(view)
        presenter.onViewReady()
        val initialRefreshCount = failingCatalog.refreshCount
        presenter.onRetryClick()
        Assert.assertTrue(failingCatalog.refreshCount > initialRefreshCount)
    }

    @Test
    fun testAddToCart() {
        val cartObserver = TestObserver<List<Product>>()
        cart.items.subscribe(cartObserver)
        presenter.attachView(view)
        presenter.onViewReady()
        Assert.assertTrue(cartObserver.values().last().isEmpty())
        presenter.onAddToCartClick(margherita)
        Assert.assertTrue(cartObserver.values().last().isNotEmpty())
    }

    @Test
    fun testOpenCart() {
        presenter.attachView(view)
        Assert.assertFalse(view.isCartOpened)
        presenter.onCartActionClick()
        Assert.assertTrue(view.isCartOpened)
    }

    @Test
    fun testCartCounter() {
        presenter.attachView(view)
        presenter.onViewReady()
        Assert.assertTrue(view.cartItemsCounter == 0)
        presenter.onAddToCartClick(margherita)
        presenter.onAddToCartClick(margherita)
        Assert.assertTrue(view.cartItemsCounter == 2)
    }

    class FakePizzaListView : PizzaListView {

        var data: List<Pizza>? = null
        var error: Throwable? = null
        var isCartOpened = false
        var isPizzaEditorOpened = false
        var isPizzaConstructorOpened = false
        var cartItemsCounter = -1

        override fun showPizzaList(data: List<Pizza>) {
            this.data = data
        }

        override fun showError(error: Throwable) {
            this.error = error
        }

        override fun openCart() {
            isCartOpened = true
        }

        override fun setCartCounter(value: Int) {
            cartItemsCounter = value
        }

        override fun openPizzaEditor(pizza: Pizza) {
            isPizzaEditorOpened = true
        }

        override fun openPizzaConstructor(basePrice: Double) {
            isPizzaConstructorOpened = true
        }
    }
}